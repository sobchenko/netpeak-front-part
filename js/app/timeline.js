function TimeLine () {

    this.$el = $('[data-timeline-items]');

    this.timelineTpl = this.$el.find('[data-id]').get(0).outerHTML;

    this.$el.empty();

    this.getData();

    setInterval(function(){

        this.getData();

    }.bind(this), 5000);
}

TimeLine.prototype = {

    getData :function () {
        $.ajax({

            type : 'GET',

            url : '/api/timeline/twitter'

        })
            .done(function (response) {

                this.timeline = response;

                if (this.timelineOld === undefined) {

                    this.buildTimeLineItemList();

                } else {

                    this.checkTimeLineItemList();

                }

            }.bind(this))

            .fail(function (error) {

                console.log(error.statusText);

            });
    },

    buildTimeLineItemList : function () {

        this.timeline.forEach(this.createTimeLineItem.bind(this));

        this.replaceOldTimeline();

    },

    createTimeLineItem : function (timeLineItemData) {

        var newItem = new TimeLineItem(this.timelineTpl, timeLineItemData);

        this.$el.append(newItem.$el);

    },

    checkTimeLineItemList : function () {

        this.removeOutdatedItems();

        this.buildTimeLineItemList();

    },

    removeOutdatedItems : function () {

        this.timelineOld.forEach(function(element) {

            this.$el.find('[data-id="' + element.id +'"]').remove();

        }.bind(this));

    },

    replaceOldTimeline : function () {

        this.timelineOld = this.timeline;

    }

}
