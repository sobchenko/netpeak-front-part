function TimeLineItem(tpl, modelData) {

    this.$el = $(tpl);

    this.model = modelData;

    this.tpl = {

        timeLineIitem : this.$el,

        publisherLogo : this.$el.find('[data-publisher-logo]'),

        publisherName : this.$el.find('[data-publisher-name]'),

        publisherUrl : this.$el.find('[data-publisher-url]'),

        publishedAgo : this.$el.find('[data-published-ago]'),

        newsText : this.$el.find('[data-news-text]'),

        newsUrl : this.$el.find('[data-news-url]'),

        republishedCount : this.$el.find('[data-republished-count]'),

        favoriteCount : this.$el.find('[data-favorite-count]')
    };

    this.render();
}

TimeLineItem.prototype = {

    render : function () {

        this.tpl.timeLineIitem.attr('data-id', this.model.id);

        this.tpl.publisherLogo.prop('src', this.model.publisher_logo);

        this.tpl.publisherLogo.prop('alt', this.model.publisher_name);

        this.tpl.publisherName.html(this.model.publisher_name);

        this.tpl.publisherUrl.prop('href', this.model.publisher_url);

        this.tpl.publisherUrl.html(this.model.publisher_timeline_name);

        this.tpl.publishedAgo.html(this.model.published_ago);

        this.tpl.newsText.html(this.model.news_text);

        this.tpl.newsText.find('a').addClass('news-url').attr('target', '_blank');

        this.tpl.newsUrl.prop('href', this.model.news_url);

        this.tpl.newsUrl.html(this.model.news_url);

        this.tpl.republishedCount.html(this.model.republished_count);

        this.tpl.favoriteCount.html(this.model.favorite_count);
    }
};